# ** RTK2INFLUX on Raspberrypi**


## 動作環境
* Raspberrypi3 [Raspbian Buster Lite(Release :2019-09-26)](https://www.raspberrypi.org/downloads/raspbian/)
* MicroSD Card 8G or more / class10 / MLC

## 事前準備
* SSHクライアントやFTPクライアント、LINUXコマンドが使えることが前提条件になります。
* SSHの許可や言語の設定等を行っておいてください。  
`$ sudo apt-get install raspi-config`  
`$ sudo raspi-config` 

## 各種ソフトウェア Install
`$ sudo apt-get update -y`  
`$ sudo apt-get upgrade -y`  
`$ sudo apt-get install python3-pip -y`  
`$ sudo pip3 install python-geohash`  
`$ sudo pip3 install influxdb`  
`$ sudo apt-get install python3-pandas -y`  
`$ sudo apt-get install screen -y`  
`$ sudo apt-get install git`  

## RTKLIB Install
`$ sudo git clone https://github.com/tomojitakasu/RTKLIB.git`  
`$ cd RTKLIB/app`  
`$ sudo chmod 755 makeall.sh`  
`$ sudo ./makeall.sh`  
`$ cd  `  
`$ cd RTKLIB/app/rtkrcv/gcc/`  
`$ sudo chmod 755 rtkstart.sh`  
`$ sudo chmod 755 rtkshut.sh`  
`$ cd`  


# rtk2influx Install
`$ cd `  
`$ sudo git clone https://bigbearfarm@bitbucket.org/bigbearfarm/rtk2influx.git`  

## rtk2influx コフィグファイル設定
[WIKIを参照](https://bitbucket.org/bigbearfarm/rtk2influx/wiki/%E8%A8%AD%E5%AE%9A%E3%83%95%E3%82%A1%E3%82%A4%E3%83%AB)  
[`$ sudo nano rtkrcv2influx.cfg`](https://bitbucket.org/bigbearfarm/rtk2influx/wiki/%E8%A8%AD%E5%AE%9A%E3%83%95%E3%82%A1%E3%82%A4%E3%83%AB)  

### rtk2influx 自動起動設定(Rorver用)  
`$ sudo cp rtkrcv2influx.service /etc/systemd/system`  
`$ sudo systemctl start rtkrcv2influx.service`  
`$ sudo systemctl enable rtkrcv2influx.service`  

### str2str 自動起動設定(Base局用)  
`$ cd `  
`$ sudo chmod +x -R rtk2influx`  
`$ cd rtk2influx`  
`$ sudo cp str2str.service /etc/systemd/system`  
`$ sudo systemctl start str2str.service`  
`$ sudo systemctl enable str2str.service`  




