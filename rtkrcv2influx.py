#----------------------------------------------------------
# Name: rtkrcv2influx.py
# Purpose: RTK kinematic data to influxdb
# referencd :
# Author:      ookuma yousuke
#
# Created: 2019/10/18
# Copyright:   (c) ookuma 2019
# Licence:     MIT License(X11 License)
#
#----------------------------------------------------------
#!/usr/bin/python3
import socket ,os,time
import serial,geohash,subprocess,configparser
from influxdb import InfluxDBClient,DataFrameClient
import pandas as pd


def weeksecondstoutc(gpsweek,gpsseconds,leapseconds):
    import datetime, calendar
    datetimeformat = "%Y-%m-%d %H:%M:%S"
    epoch = datetime.datetime.strptime("1980-01-06 00:00:00",datetimeformat)
    elapsed = datetime.timedelta(days=(gpsweek*7),seconds=(gpsseconds+leapseconds))
    return datetime.datetime.strftime(epoch + elapsed,datetimeformat)

def insert_influx(df,config):
    inf_host = config['influx']['server'] 
    inf_port = int(config['influx']['port']) #8086
    user = config['influx']['user'] #root
    pwd = config['influx']['password'] #'root'
    dbname = config['influx']['db'] #'gnss'
    mesurement = config['influx']['mesurement'] #'m8t'
    protocol = 'line'
#    tags = { "Q": df[["Q"]]}
#    print("Q:")
#    print(df[["Q"]])
    client = DataFrameClient(inf_host, inf_port, user, pwd, dbname, timeout=3, retries=6 )

    try:
        print("insert influxdb : start")
        client.write_points(df,mesurement, tags=None, protocol=protocol )
        print("insert influxdb : end")
    except :
        pass

def chk_rtkrcv(config):
    print("check rtkrcv start.............")

    while True:
        # cmd = "ps aux | grep -c rtkrcv"
        p1 = subprocess.Popen(["ps","aux"], stdout=subprocess.PIPE)
        p2 = subprocess.Popen(["grep", "-c","rtkrcv_run"], stdin=p1.stdout, stdout=subprocess.PIPE)
        p1.stdout.close()
        cnt = p2.communicate()[0].decode('utf-8')
        print("Now Conecting......" + str(cnt))
        time.sleep(3)
         # screen -AmdS rtkrcv ./rtkrcv  -o /home/pi/my.conf -s
        if int(cnt) <= 1:
#            rtkrcv = "/home/pi/RTKLIB/app/rtkrcv/gcc/rtkrcv"
            rtkrcv = config['rtklib']['rtkrcv_dir']
            rtk_conf = config['rtklib']['rtkrcv_conf']
            print("Execute rtkrcv .......")
            cmd = "screen -AmdS rtkrcv_run " + rtkrcv + " -o " + rtk_conf + " -s "
            print(cmd)
            subprocess.Popen(cmd.split(), stdout=subprocess.PIPE)
            break
        elif int(cnt) >1:
            break

def rtk_rev(config):
#    host = '127.0.0.1' #localhost
    host = config['rtklib']['rtkrcv_tcpsvr']
    port = int(config['rtklib']['rtkrcv_port'])
    bufsize = 150
#    time.sleep(30)
    print("Rtkrcv_rec Execute")
    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((host, port))
        ser = serial.Serial('/dev/serial0',115200,timeout=3)
#        i=0
        llh_sum=[]
        llh_list=[]
        index_time = []
        while True:
            savepoint = sock.recv(bufsize)
#            print(savepoint.decode(encoding='utf-8'))
            llh_list=savepoint.decode(encoding='utf-8').split()
            # time
            if ('.' not in llh_list[0]) and ('.' not in llh_list[1]):
#                print(llh_list[0],llh_list[1])
#                print(savepoint.decode(encoding='utf-8'))
                gpsweek = int(llh_list[0])
                gpsseconds = int(llh_list[1])
                leapseconds = 0
                utc_time = weeksecondstoutc(gpsweek,gpsseconds,leapseconds)
            else:
                print("skip")
                continue
            # create index list
            index_time.append(pd.to_datetime(utc_time))
            # create llh_sum
            a=[]
            for ii in range(2,15):
                a.append(float(llh_list[ii]))
#                a.append('{:.10g}'.format(llh_list[i]))
            a.append(geohash.encode(float(llh_list[2]),float(llh_list[3])))
            llh_sum.append(a)
            print(a)
            if len(llh_sum)>15:
               # dataflame
                df = pd.DataFrame(
                         llh_sum
                        ,index = index_time
                        ,columns=["lat","lon","heigt","Q","ns","sdn","sde","sdu","sdne","sdeu","sdun","age","ratio","hash"]
                        )

                print(df)
               # insert influx
                insert_influx(df,config)
               # del llh_sum
                index_time.clear()
                llh_sum.clear()
                del df

#            i +=1
#            print('No:' + str(i))
        ser.close()
        sock.close()

    except socket.error:
        print('socket error')

# read config
filepath = os.path.dirname(os.path.abspath(__file__))
filepath = filepath + '/rtkrcv2influx.cfg' #CNF
config = configparser.ConfigParser()
config.read(filepath)

chk_rtkrcv(config)
time.sleep(10)
rtk_rev(config)
